<?php

/**
 * ibVPN account configuration.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage views
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 Darryl Sokoloski
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/date/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('ibvpn');

///////////////////////////////////////////////////////////////////////////////
// Form handler
///////////////////////////////////////////////////////////////////////////////

if ($form_type === 'edit') {
    $read_only = FALSE;
    $buttons = array( 
        form_submit_update('submit-form'),
        anchor_cancel('/app/ibvpn')
    );
} else {
    $read_only = TRUE;
    $buttons = array( 
        anchor_edit('/app/ibvpn/account/edit')
    );
}

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

echo form_open('ibvpn/account/edit', array('id' => 'account_form', 'autocomplete' => 'off'));
echo form_header(lang('base_account'), array('id' => 'account'));
if ($read_only === FALSE || !strlen($username)) {
    echo form_banner("<p>" . lang('ibvpn_account_desc') . " <b><a href='http://www.ibvpn.com/billing/aff.php?aff=1088' target='_blank'>ibVPN Website</a></b>.</p>");
}
if ($read_only) {
    if (strlen($username))
        echo field_input('username', $username, lang('ibvpn_username'), $read_only);
}
else
    echo field_input('username', $username, lang('ibvpn_username'), $read_only);
if ($read_only === FALSE) {
    echo form_banner("<p>" . lang('ibvpn_password_help') . " <a href='https://www.ibvpn.com/billing/clientarea.php?action=details' target='_blank'>ibVPN Client Area</a>.</p>");
    echo field_password('password', $password, lang('ibvpn_password'), $read_only);
}

echo field_button_set($buttons);

echo form_footer();
echo form_close();

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
