<?php

/**
 * ibVPN class.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage libraries
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 Darryl Sokoloski
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\ibvpn;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ?
    getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';

require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('ibvpn');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Folder as Folder;
use \clearos\apps\base\Shell as Shell;
use \clearos\apps\base\Webconfig as Webconfig;

clearos_load_library('base/Engine');
clearos_load_library('base/File');
clearos_load_library('base/Folder');
clearos_load_library('base/Shell');
clearos_load_library('base/Webconfig');

// Exceptions
//-----------

use \Exception as Exception;
use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\File_Not_Found_Exception as File_Not_Found_Exception;
use \clearos\apps\base\Folder_Not_Found_Exception as Folder_Not_Found_Exception;
use \clearos\apps\base\File_No_Match_Exception as File_No_Match_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Engine_Exception');
clearos_load_library('base/File_Not_Found_Exception');
clearos_load_library('base/Folder_Not_Found_Exception');
clearos_load_library('base/File_No_Match_Exception');
clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ibVPN class.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage libraries
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ibvpn/
 */

class Ib_Vpn extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const FILE_CONFIG = '/etc/clearos/ibvpn.conf';
    const FILE_ACCOUNT = '/etc/clearos/ibvpn.d/passwd';
    const FILE_CERT = 'ibvpn.com.crt';
    const FILE_STATE = '/var/state/webconfig/ibvpn.state';
    const FILE_TEMPLATE = '/usr/clearos/apps/ibvpn/deploy/template.ovpn';

    const PATH_CONFIG = '/etc/clearos/ibvpn.d';
    const PATH_TEMP = '/var/clearos/framework/tmp';

    const URL_UPDATE = 'http://download.ibvpn.com/openvpn.tar.gz';

    const SCRIPT_ROUTE = '/etc/clearos/ibvpn.d/route-up.sh';

    const METHOD_HEAD = 0;
    const METHOD_GET = 1;

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * ibVPN constructor.
     *
     * @return void
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Set account credentials.
     *
     * @param string $username username (email address)
     * @param string $password password
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_account($username, $password)
    {
        $file = new File(self::FILE_ACCOUNT);
        if ($file->exists()) $file->delete();
        $file->create('root', 'root', '0600');
        $file->dump_contents_from_array(array($username, $password));
    }

    /**
     * Get account credentials.
     *
     * @return array array containing credentials
     * @throws Engine_Exception
     */

    public function get_account()
    {
        $file = new File(self::FILE_ACCOUNT);
        if (! $file->exists()) return FALSE;
        $lines = $file->get_contents_as_array();
        if (count($lines) < 2) return FALSE;
        $username = trim($lines[0]);
        $password = trim($lines[1]);
        if (strlen($username) == 0 || strlen($password) == 0)
            return FALSE;
        return array('username' => $username, 'password' => $password);
    }

    /**
     * Set ibVPN server.
     *
     * @param string $server ibVPN server (configuration file)
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_server($server)
    {
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists())
            $file->create('root', 'root', '0644');
        try {
            $file->replace_one_line(
                '/^ibvpn_conf/', "ibvpn_conf=\"$server\"\n");
        } catch (File_No_Match_Exception $e) {
            $file->add_lines("ibvpn_conf=\"$server\"\n");
        }
    }

    /**
     * Get ibVPN server.
     *
     * @return string ibVPN server
     * @throws Engine_Exception
     */

    public function get_server()
    {
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) return FALSE;
        try {
            $server = $file->lookup_line('/^ibvpn_conf/');
        } catch (File_No_Match_Exception $e) {
            return FALSE;
        }

        $server = preg_replace('/ibvpn_conf\s*=\s*/', '', $server);
        return preg_replace('/"/', '', $server);
    }

    /**
     * Get list of all ibVPN servers.
     *
     * @return array array containing ibVPN servers
     * @throws Engine_Exception
     */

    public function get_server_list()
    {
        $servers = array();
        $folder = new Folder(self::PATH_CONFIG);
        if (! $folder->exists()) return $servers;
        $files = $folder->get_listing();
        foreach ($files as $file) {
            if (! preg_match('/\.ovpn$/', trim($file))) continue;
            $servers[] = preg_replace('/\.ovpn/', '', $file);
        }
        return $servers;
    }

    /**
     * Update ibVPN servers.
     *
     * @return string JSON encoded string containing update status.
     * @throws Engine_Exception
     */

    public function update_servers()
    {
        // HEAD /openvpn.tar.gz HTTP/1.1
        // User-Agent: app-ibvpn/1.0
        // Host: download.ibvpn.com
        // Accept: */*
        // Referer: http://clearcenter.com/

        // HTTP/1.1 200 OK
        // Date: Tue, 19 Mar 2013 22:48:11 GMT
        // Server: Apache/2.2.15 (CentOS)
        // Last-Modified: Wed, 13 Mar 2013 12:35:22 GMT
        // ETag: "10609b4-ca9-4d7cda3ba3680"
        // Accept-Ranges: bytes
        // Content-Length: 3241
        // Connection: close
        // Content-Type: application/x-gzip

        $json = array(
            'success' => FALSE,
            'status' => lang('ibvpn_status_none'));

        $ch = curl_init(self::URL_UPDATE);

        curl_setopt($ch, CURLOPT_REFERER, 'http://clearcenter.com/');
        curl_setopt($ch, CURLOPT_USERAGENT, 'app-ibvpn/1.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);

        // Check for upstream proxy settings
        //----------------------------------

        if (clearos_app_installed('upstream_proxy')) {
            clearos_load_library('upstream_proxy/Proxy');

            $proxy = new \clearos\apps\upstream_proxy\Proxy();

            $proxy_server = $proxy->get_server();
            $proxy_port = $proxy->get_port();
            $proxy_username = $proxy->get_username();
            $proxy_password = $proxy->get_password();

            if (! empty($proxy_server))
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);

            if (! empty($proxy_port))
                curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);

            if (! empty($proxy_username)) {
                curl_setopt($ch, CURLOPT_PROXYUSERPWD,
                    $proxy_username . ':' . $proxy_password);
            }
        }

        $etag = NULL;
        $etag_prev = NULL;

        $file = new File(self::FILE_STATE, TRUE);
        try {
            if ($file->exists())
                $etag_prev = $file->get_contents();
        }
        catch (Exception $e) { }

        for ($method = self::METHOD_HEAD;
            $method < self::METHOD_GET + 1; $method++) {
            switch ($method) {
            case self::METHOD_HEAD:
                curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
                curl_setopt($ch, CURLOPT_NOBODY, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, TRUE);
                break;

            case self::METHOD_GET:
                curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
                curl_setopt($ch, CURLOPT_NOBODY, FALSE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                break;
            }

            $result = curl_exec($ch);

            if ($result === FALSE) {
                $json['status'] = curl_error($ch);
                return json_encode($json);
            }

            if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
                $json['status'] = lang('ibvpn_status_unexpected_result');
                return json_encode($json);
            }

            switch ($method) {
            case self::METHOD_HEAD:
                $headers = explode("\r\n", $result);
                foreach ($headers as $header) {
                    if (! preg_match('/^ETag:\s+"([0-9A-f-]+)"/',
                        $header, $matches)) continue;

                    $etag = $matches[1];
                    break;
                }

                if ($etag !== NULL && $etag_prev !== NULL &&
                    $etag == $etag_prev) {
                    $json['success'] = TRUE;
                    $json['status'] = lang('ibvpn_status_up_to_date');
                    return json_encode($json);
                }

                break;

            case self::METHOD_GET:
                try {
                    if ($file->exists()) $file->delete();
                    $file->create('webconfig', 'webconfig', '0644');
                    $file->add_lines($matches[1]);

                    $file = new File(self::PATH_TEMP . '/openvpn.tar.gz', TRUE);
                    if ($file->exists()) $file->delete();
                    $file->create('webconfig', 'webconfig', '0644');
                    $file->add_lines($result);

                } catch (Exception $e) { }
                break;
            }
        }

        if (! $file->exists()) {
            $json['status'] = lang('ibvpn_status_download_failed');
            return json_encode($json);
        }

        $folder = new Folder(self::PATH_TEMP . '/openvpn');
        if ($folder->exists()) $folder->delete(TRUE);

        $shell = new Shell();
        try {
            $shell->execute('/bin/tar', '-xzf ' . self::PATH_TEMP .
                '/openvpn.tar.gz -C ' . self::PATH_TEMP);
        }
        catch (Exception $e) {
            $json['status'] = lang('ibvpn_status_update_failed');
            return json_encode($json);
        }

        if (! $folder->exists()) {
            $json['status'] = lang('ibvpn_status_update_corrupt');
            return json_encode($json);
        }

        try {
            $servers_new = array();
            $files = $folder->get_listing();
            foreach ($files as $file) {
                if (! preg_match('/\.ovpn$/', trim($file))) continue;
                $servers_new[] = preg_replace(
                    array('/ibVPN-/', '/\.ovpn/'), array('', ''), $file);
            }

            $servers_old = $this->get_server_list();
            $servers_diff = array_diff($servers_old, $servers_new);
            foreach ($servers_diff as $server) {
                $file = new File(self::PATH_CONFIG . "/$server.ovpn", TRUE);
                $file->delete();
            }

            $file = new File(self::FILE_TEMPLATE);
            $template = $file->get_contents_as_array();
            foreach ($servers_new as $server) {
                $remote = array("# $server");
                $file = new File(self::PATH_TEMP . "/openvpn/ibVPN-$server.ovpn");
                $contents = $file->get_contents_as_array();
                foreach ($contents as $line) {
                    if (! preg_match('/^remote/', $line)) continue;
                    $remote[] = trim($line);
                }
                $remote[] = '';
                $contents = array();
                foreach ($remote as $addr)
                    $contents[] = $addr;
                foreach ($template as $line)
                    $contents[] = $line;
                $file = new File(self::PATH_CONFIG . "/$server.ovpn", TRUE);
                if (! $file->exists()) $file->create('root', 'root', '0644');
                $file->dump_contents_from_array($contents);
            }

            $file = new File(self::PATH_TEMP .
                '/openvpn/' . self::FILE_CERT, TRUE);
            if ($file->exists())
                $file->copy_to(self::PATH_CONFIG . '/' . self::FILE_CERT);

            $folder->delete(TRUE);
        
            $json['success'] = TRUE;
            $json['status'] = lang('ibvpn_status_update_success');
        }
        catch (Exception $e) {
            $json['status'] = lang('ibvpn_status_update_exception');
        }

        return json_encode($json);
    }

    /**
     * Add device (by IP address) to route through ibVPN.
     *
     * @param string $ip IP address of device
     *
     * @return boolean FALSE if configuration doesn't exist.
     * @throws Engine_Exception
     */

    public function add_device($ip)
    {
        $file = new File(self::FILE_CONFIG, TRUE);
        if (! $file->exists()) return FALSE;
        $devices = $this->get_device_list();
        if (array_search($ip, $devices) === FALSE) {
            $devices[] = $ip;
            $lines = '';
            foreach ($devices as $key => $device)
                $lines .= "ibvpn_device[$key]=\"$device\"\n";
            $file->delete_lines('/^ibvpn_device/');
            $file->add_lines($lines);

            try {
                $shell = new Shell();
                $shell->execute(self::SCRIPT_ROUTE, '', TRUE);
            } catch (Exception $e) { }

            return TRUE;
        }

        return FALSE;
    }

    /**
     * Delete device (by IP address) from ibVPN routing rules.
     *
     * @param string $ip IP address of device to delete
     *
     * @return boolean FALSE if configuration doesn't exist.
     * @throws Engine_Exception
     */

    public function delete_device($ip)
    {
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) return FALSE;
        $devices = $this->get_device_list();
        if (($key = array_search($ip, $devices)) !== FALSE) {
            unset($devices[$key]);
            sort($devices);
            $lines = '';
            foreach ($devices as $key => $device)
                $lines .= "ibvpn_device[$key]=\"$device\"\n";
            $file->delete_lines('/^ibvpn_device/');
            $file->add_lines($lines);

            try {
                $shell = new Shell();
                $shell->execute(self::SCRIPT_ROUTE, '', TRUE);
            } catch (Exception $e) { }

            return TRUE;
        }

        return FALSE;
    }

    /**
     * Get list of all devices configured to be routed through ibVPN.
     *
     * @return array array containing devices routed through ibVPN
     * @throws Engine_Exception
     */

    public function get_device_list()
    {
        $devices = array();
        $file = new File(self::FILE_CONFIG);
        if (! $file->exists()) return $devices;
        $lines = $file->get_contents_as_array();
        foreach ($lines as $line) {
            if (! preg_match('/^ibvpn_device\[(\d+)\]\s*=\s*"(.*)"/', $line, $matches)) continue;
            $devices[] = $matches[2];
        }

        sort($devices);

        return $devices;
    }

    public function sync_routing_rules()
    {
        try {
            $shell = new Shell();
            $shell->execute(self::SCRIPT_ROUTE, '', TRUE);
        } catch (Exception $e) { }
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N   R O U T I N E S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validates a hostname or IP address.
     *
     * @param string $addr Address
     *
     * @return string error message if address is invalid
     */

    public function validate_address($addr)
    {
        clearos_profile(__METHOD__, __LINE__);

        $ip = gethostbyname($addr);
        if (inet_pton($ip) !== FALSE) return '';

        return lang('ibvpn_address_invalid');
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
