<?php

/**
 * ibVPN controller.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/mobile_demo/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\network\Network as Network;
use \Exception as Exception;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ibVPN controller.
 *
 * @category   apps
 * @package    ibvpn
 * @subpackage controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/mobile_demo/
 */

class IbVpn extends ClearOS_Controller
{
    /**
     * Index.
     */

    function index()
    {
        // Load dependencies
        //---------------

        $this->load->library('ibvpn/Ib_Vpn');
        $this->load->library('network/Network');
        $this->lang->load('ibvpn');

        // Load views
        //-----------

        $mode = $this->network->get_mode();

        if ($mode == Network::MODE_STANDALONE ||
            $mode == Network::MODE_TRUSTED_STANDALONE) {
            $this->page->view_form('ibvpn/warning', '', lang('ibvpn_ibvpn'));
        }
        else {
            $views = array('ibvpn/server', 'ibvpn/account');
            $account = $this->ib_vpn->get_account();
            if ($account !== FALSE) $views[] = 'ibvpn/location';
            $location = $this->ib_vpn->get_server();
            if ($account !== FALSE && strlen($location))
                $views[] = 'ibvpn/devices';
            $this->page->view_forms($views, lang('ibvpn_ibvpn'));
        }
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
