#!/usr/clearos/sandbox/usr/bin/php
<?php

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// U P D A T E  I B V P N  S E R V E R S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\ibvpn\Ib_Vpn as Ib_Vpn;

clearos_load_library('ibvpn/Ib_Vpn');

$ibvpn = new Ib_Vpn();

$status = json_decode($ibvpn->update_servers());

if ($status->success === TRUE) {
	printf("%s.\n", $status->status);
	exit(0);
}
else {
	printf("%s.\n", $status->status);
	exit(1);
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
