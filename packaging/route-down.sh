#!/bin/bash

# Source ibVPN configuration
. /etc/clearos/ibvpn.conf

# Flush ibVPN routing table
/sbin/ip route flush table 20 >/dev/null 2>&1

# Remove any old host routing rules
while true; do
    /sbin/ip rule del pref 20 >/dev/null 2>&1 || break
done

exit 0

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
