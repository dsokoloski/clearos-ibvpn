#!/bin/bash

# Source ibVPN configuration
. /etc/clearos/ibvpn.conf

# PID directory
piddir="/var/run/ibvpn"

# Create ibVPN routing table
if [ ! -z "$dev" -a ! -z "$route_vpn_gateway" ]; then
    /sbin/ip route flush table 20
    /sbin/ip route ls table main | grep -Ev ^default |\
        while read entry; do /sbin/ip route add $entry table 20; done
    /sbin/ip route add default via $route_vpn_gateway dev $dev table 20
else
    # Rebuild table if it exists
    route_vpn_default=$(/sbin/ip route ls table 20 | grep -E ^default)
    /sbin/ip route flush table 20
    if [ ! -z "$route_vpn_default" ]; then
        /sbin/ip route ls table main | grep -Ev ^default |\
            while read entry; do /sbin/ip route add $entry table 20; done
        /sbin/ip route add $route_vpn_default table 20
    fi
fi

# Remove any old host routing rules
while true; do
    /sbin/ip rule del pref 20 >/dev/null 2>&1 || break
done

# Create host routing rules if service is running
if [ -f "$piddir/ibvpn.pid" ]; then
    ibvpn_pid=$(cat "$piddir/ibvpn.pid")

    # Check if service is running
    if [ ! -z "$ibvpn_pid" -a -e "/proc/$ibvpn_pid" ]; then
        for i in "${!ibvpn_device[@]}"; do

            for ifn in "$(network --get-lan-interfaces)"; do
                # Avoid device being redirected through HTTP/S proxy
                /sbin/iptables -t nat -D PREROUTING \
                    -i $ifn -s ${ibvpn_device[$i]} -p tcp \
                    --dport 80 -j ACCEPT >/dev/null 2>&1
                /sbin/iptables -t nat -D PREROUTING \
                    -i $ifn -s ${ibvpn_device[$i]} -p tcp \
                    --dport 443 -j ACCEPT >/dev/null 2>&1
                /sbin/iptables -t nat -I PREROUTING \
                    -i $ifn -s ${ibvpn_device[$i]} -p tcp \
                    --dport 80 -j ACCEPT
                /sbin/iptables -t nat -I PREROUTING \
                    -i $ifn -s ${ibvpn_device[$i]} -p tcp \
                    --dport 443 -j ACCEPT
            done

            # Add alternate table look-up rule
            /sbin/ip rule add from ${ibvpn_device[$i]} pref 20 table 20
        done
    fi
fi

exit 0

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
