#!/bin/sh

# OpenVPN configlet directory
ibvpn_confd="/etc/clearos/ibvpn.d"

# Location of ibvpn symlink to openvpn
ibvpn="/usr/sbin/ibvpn"

# PID directory
piddir="/var/run/ibvpn"

# Source configuration file
. /etc/clearos/ibvpn.conf

# Stop service, if running...
if [ -f "$piddir/ibvpn.pid" ]; then
    ibvpn_pid=$(cat "$piddir/ibvpn.pid")
    # Check if service is running
    if [ ! -z "$ibvpn_pid" -a -e "/proc/$ibvpn_pid" ]; then
        kill $ibvpn_pid
        while [ -d "/proc/$ibvpn_pid" ]; do
            sleep 1
        done
        exit 0
    else
        exit 1
    fi
else
    exit 1
fi

while true; do ip rule del pref 20 >/dev/null 2>&1 || break; done
ip route flush table 20 >/dev/null 2>&1

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4 syntax=sh
