#!/bin/sh

# OpenVPN configlet directory
ibvpn_confd="/etc/clearos/ibvpn.d"

# Location of ibvpn symlink to openvpn
ibvpn="/usr/sbin/ibvpn"

# PID directory
piddir="/var/run/ibvpn"

# Source configuration file
. /etc/clearos/ibvpn.conf

# Load tun module
/sbin/modprobe tun >/dev/null 2>&1

# Ensure we've been configured
if [ -z "$ibvpn_conf" ]; then
    exit 1
fi

# Create PID dir if it doesn't exist
if [ ! -d  $piddir ]; then
    mkdir $piddir
fi

# Check if PID file exists
if [ -f "$piddir/ibvpn.pid" ]; then
    ibvpn_pid=$(cat "$piddir/ibvpn.pid")
    # Check if service is already running
    if [ ! -z "$ibvpn_pid" -a -e "/proc/$ibvpn_pid" ]; then
        exit 1
    fi
    # Remove stale PID file
    rm -f "$piddir/ibvpn.pid"
fi

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4 syntax=sh
