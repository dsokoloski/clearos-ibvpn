#!/bin/sh

# OpenVPN configlet directory
ibvpn_confd="/etc/clearos/ibvpn.d"

# Location of ibvpn symlink to openvpn
ibvpn="/usr/sbin/ibvpn"

# PID directory
piddir="/var/run/ibvpn"

# Source configuration file
. /etc/clearos/ibvpn.conf

# Start service
ln -sf /usr/sbin/openvpn "$ibvpn"
$ibvpn --daemon ibvpn --config "$ibvpn_confd/$ibvpn_conf.ovpn" --cd "$ibvpn_confd"

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4 syntax=sh
